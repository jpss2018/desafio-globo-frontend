import React from "react"
import { DebounceInput } from "react-debounce-input"

export type Filter = {
    placeholder: string,
    value: string | number | undefined,
    handleChange(query: string): void
}

type IProps = {
    filters: Filter[]
}

const FiltersComponent = ({ filters = [] }: IProps): JSX.Element => {

    return (
        <div className="row">
            {
                filters.map(({ placeholder, handleChange, value }, index) => {
                    return <div key={index} className="col-md">
                        <DebounceInput
                            className="form-control"
                            placeholder={placeholder}
                            value={value}
                            minLength={2}
                            debounceTimeout={500}
                            onChange={event => handleChange(event.target.value)} />
                    </div>
                })
            }
        </div>
    )
}

export default React.memo(FiltersComponent)