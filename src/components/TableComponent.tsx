import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Table } from 'react-bootstrap'

export type Column = {
    title: string,
    path: string
}

type IProps = {
    data: any[]
    columns: Column[],
}

export const TableComponent = ({ columns = [], data = [] }: IProps): JSX.Element => {

    const [tableArray, setTableArray] = useState<any>([[]]);

    useEffect(() => {
        buildArray(columns, data)
    }, [columns, data])

    const buildArray = (columns: Column[], data: any[]) => {
        const array: any = []
        columns.forEach(column => {
            array[column.title] = []

            data.forEach((ele, index) => {
                array[column.title][index] = ele
            })
        })

        setTableArray(array);
    }

    return (
        <Table className="table">
            <thead className="thead-dark">
                <StyledTr>
                    {
                        columns.map(column => {
                            return (<th>{column?.title}</th>)
                        })
                    }
                </StyledTr>
            </thead>
            <tbody>
                {
                    data.map((ele, index) => {
                        return <tr key={index}>
                            {
                                columns.map(({ title, path }, index2) => {

                                    let cell;

                                    if (tableArray[title] && tableArray[title][index]) {
                                        const ele = tableArray[title][index]
                                        cell = ele[path]
                                    }

                                    return (<td key={title + index2}>{cell}</td>)
                                })
                            }

                        </tr>
                    })
                }
            </tbody>
        </Table>
    )
}

export default React.memo(TableComponent);

const StyledTr = styled.tr`
    text-transform: upperCase;
    
    th{
        white-space: nowrap;
    }
`