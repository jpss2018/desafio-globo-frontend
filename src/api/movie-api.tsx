import axiosInstance from './axiosInstance';

export const get = (title?: string, director?: string, description?: string, releaseDate?: string) => {
    return axiosInstance.get("/movie", {
        params: {
            title,
            director,
            description,
            releaseDate
        }
    });
}
