
import axios from 'axios';

const headers = {
    "Access-Control-Allow-Origin": "*"
}

const axiosInstance = axios.create({
    baseURL: 'http://www.desafio-globo.jpss.com.br/backend-globo-challenge-api/rest',
    headers
});

axiosInstance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    if (token) config.headers.Authorization = "Bearer " + token;
    return config;
});

axiosInstance.interceptors.response.use(
    response => {
        return response
    },
    error => {
        if (error.response.config.url !== '/auth' && error.response.status === 401) {
            clearStorage();
        }
        return Promise.reject(error);
    });

export default axiosInstance;

export const clearStorage = () => {
    localStorage.removeItem("token")
    localStorage.removeItem("currentUser")
    window.location.replace("/")
}