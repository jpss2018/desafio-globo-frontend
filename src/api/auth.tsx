import axiosInstance from './axiosInstance';

export const logIn = (username: string, password: string) => {
    return axiosInstance.post("/auth", {}, {
        params: {
            username,
            password
        }
    });
}

export const logOut = () => {
    return axiosInstance.post("/auth/logout", {});
}
