import axiosInstance from './axiosInstance';

export const findUser = (id: number) => {
    return axiosInstance.get(`/user/${id}`);
}
