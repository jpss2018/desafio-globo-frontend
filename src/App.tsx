import { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import LoginComponent from './pages/LoginComponent/LoginComponent';
import MainComponent from './pages/MainComponent';
import './app.scss'

const App = () => {

    const [token, setToken] = useState<string | null>()

    useEffect(() => {
        const token = localStorage.getItem("token");
        setToken(token)
    }, [])

    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <LoginComponent callback={setToken} />
                </Route>

                {
                    token &&
                    <>
                        <Route path="/main">
                            <MainComponent />
                        </Route>
                    </>
                }
            </Switch>
        </Router>
    );
}

export default App;
