import React, { useState } from "react"
import { Redirect } from "react-router"
import { logIn } from "../../api/auth"
import './login.scss'
import logoGlobo from './../../assets/logo-rede-globo.png'

const LoginComponent = ({ callback }: any) => {

    const [fields, setFields] = useState<any>({ username: '', password: '' })
    const [token, setToken] = useState<any>()
    const [passwordWrong, setPasswordWrong] = useState<boolean>(false)

    const authenticate = (event: any): void => {
        logIn(fields.username, fields.password)
            .then(response => {
                if (response.status === 200) {
                    const { user: { id } } = response.data
                    const tokenAux = response.data.token
                    localStorage.setItem('token', tokenAux)
                    localStorage.setItem('currentUser', id)
                    setToken(tokenAux)
                    callback(tokenAux)
                    setPasswordWrong(false)
                }
            })
            .catch(error => {
                console.log('PASSWORD WRONG')
                setPasswordWrong(true)
            })
        event.preventDefault();
    }

    return (
        <div className="login">
            <div className="wrapper fadeInDown">
                <div id="formContent">
                    <div className="fadeIn first">
                        <img src={logoGlobo} id="icon" alt="User Icon" />
                    </div>
                    <form onSubmit={authenticate}>
                        <input type="text" id="login" className="fadeIn second" name="username" placeholder="login" value={fields.username} onChange={(event) => setFields({ ...fields, ...{ username: event.target.value } })} />
                        <input type="password" id="password" className="fadeIn third" name="password" placeholder="password" value={fields.password} onChange={(event) => setFields({ ...fields, ...{ password: event.target.value } })} />
                        <input type="submit" className="fadeIn fourth" value="Log In" />
                    </form>

                    {
                        passwordWrong &&
                        <div className="alert alert-danger" role="alert">
                            Incorrect password. Try again
                        </div>
                    }

                </div>
            </div>

            {token &&
                <Redirect
                    to={{
                        pathname: "/main"
                    }}
                />
            }
        </div>

    )
}

export default React.memo(LoginComponent);