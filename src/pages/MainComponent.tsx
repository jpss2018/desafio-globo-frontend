import React, { useCallback, useEffect, useState } from "react"
import DataTable from "react-data-table-component";
import FiltersComponent, { Filter } from "../components/FiltersComponent";
import { get } from './../api/movie-api';
import styled from 'styled-components'
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { findUser } from './../api/user';
import { logOut } from "../api/auth";
import { clearStorage } from "../api/axiosInstance";

const columns = [
    {
        name: 'Title',
        selector: (row: any) => row.title,
        sortable: true,
    },
    {
        name: 'Director',
        selector: (row: any) => row.director,
        sortable: true,
    },
    {
        name: 'Release Date',
        selector: (row: any) => row.release_date,
        sortable: true,
    },
    {
        name: 'Description',
        selector: (row: any) => row.description,
        sortable: true,
    },
    {
        name: 'People',
        selector: (row: any) => row.personStr,
        sortable: true,
    },
    {
        name: 'Score',
        selector: (row: any) => row.rt_score,
        sortable: true,
    }
];


const MainComponent = () => {

    const [movies, setMovies] = useState<any[]>([]);
    const [queryTitle, setQueryTitle] = useState<string>();
    const [queryDirector, setQueryDirector] = useState<string>();
    const [queryReleasedDate, setQueryReleasedDate] = useState<string>();
    const [queryDescription, setQueryDescription] = useState<string>();
    const [pending, setPending] = useState<boolean>(true);
    const [user, setUser] = useState<any>()

    const filters: Filter[] = [
        {
            placeholder: 'Search by title of movie',
            value: queryTitle,
            handleChange: setQueryTitle
        },
        {
            placeholder: 'Search by description of movie',
            value: queryDescription,
            handleChange: setQueryDescription
        },
        {
            placeholder: 'Search by director',
            value: queryDirector,
            handleChange: setQueryDirector
        },
        {
            placeholder: 'Search by realesed date',
            value: queryReleasedDate,
            handleChange: setQueryReleasedDate
        }
    ]

    const joinPeople = useCallback((movies: any[]): void => {
        if (movies) {
            movies = movies?.map(movie => {
                movie.personStr =
                    movie.person
                        .map((person: any) => {
                            return person.name;
                        })
                        .join(", ");

                return movie
            })
        }
    }, []);

    const getMovies = useCallback((queryTitle?: string, queryDirector?: string, queryDescription?: string, queryReleasedDate?: string): void => {
        get(queryTitle, queryDirector, queryDescription, queryReleasedDate).then(response => {
            const data = response.data
            joinPeople(data);
            setMovies(current => data);
            setPending(false)
        })
    }, [setMovies, joinPeople]);

    const handleLogout = () => {
        logOut().then(response => {
            clearStorage();
        })
    }

    useEffect(() => {
        const currentUserID: any = localStorage.getItem('currentUser')

        if (currentUserID) {
            findUser(currentUserID).then(response => {
                setUser(response.data)
            })
        }
    }, [])

    useEffect(() => {
        setPending(true)
        getMovies(queryTitle, queryDirector, queryDescription, queryReleasedDate);
    }, [queryDirector, queryTitle, queryReleasedDate, queryDescription, getMovies])

    return (
        <>
            <Navbar variant="dark" bg="dark" expand="lg">
                <Container fluid>
                    <Navbar.Brand href="#home"> Globo Challenge</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbar-dark-example" />
                    <Navbar.Collapse id="navbar-dark-example">
                        <Nav>
                            <NavDropdown
                                id="nav-dropdown-dark-example"
                                title={user?.username}
                                menuVariant="dark"
                            >
                                <NavDropdown.Item onClick={handleLogout}>Logout</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <br />

            <div className="container">
                <FiltersComponent filters={filters} />
                <br />

                <StyledDataTable>
                    <DataTable
                        fixedHeader
                        fixedHeaderScrollHeight={"550px"}
                        columns={columns} data={movies} pagination progressPending={pending}
                        paginationPerPage={10} paginationRowsPerPageOptions={[10, 20, 30, 40, 50, 60]} />
                </StyledDataTable>
            </div>
        </>
    )
}

export default React.memo(MainComponent);


const StyledDataTable = styled.div`
    .rdt_TableCell{
        div{
            white-space: initial;
        }

        &:nth-child(4){
            flex-grow: 2
        }

        &:nth-child(5){
            flex-grow: 2
        }
    }

    .rdt_TableCol{
        &:nth-child(4){
            flex-grow: 2
        }

        &:nth-child(5){
            flex-grow: 2
        }
    }
`
