# Globo challenge
This project aims to implement the challenges proposed in the Globo selection process.

Technologies used to implement the project were:
- ReactJS, SCSS, Typescript, Bootstrap

To log into the system, enter the following credentials:
username: `admin`
passowrd: `admin`

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Deployment

### `npm run build`

